package com.example.mycomposeview

import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.navigation.NavHostController

object PresentScreen : BaseScreen() {
    @Composable
    public fun Base(navHostController: NavHostController, route: String) {
        navHost(navController = navHostController, route)
    }
}