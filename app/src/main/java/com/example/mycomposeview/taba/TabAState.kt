package com.example.mycomposeview.taba

data class TabAState (
    var text:String = "",
    var showExitDialogMutableState:Boolean = false
)