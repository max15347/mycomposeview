package com.example.mycomposeview.taba

import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow

class TabAViewModel : ViewModel() {
    private val _tabAState = MutableStateFlow(TabAState())
    val tabAState: StateFlow<TabAState> = _tabAState
    fun updateText(newText: String) {
        val currentTabState = _tabAState.value
        _tabAState.value = currentTabState.copy(text = newText)
    }

    fun updateShowExitDialogMutableState(b:Boolean) {
        val currentTabState = _tabAState.value
        _tabAState.value = currentTabState.copy(showExitDialogMutableState = b)
    }

    public fun getUrlResponse() {
        Log.e("wtf", "getUrlResponse")
        Thread {
            Thread.sleep(3000)
            updateText("gg")
        }.start()
    }
}