package com.example.mycomposeview.taba

import android.annotation.SuppressLint
import android.app.Activity
import android.util.Log
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.Column
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.example.mycomposeview.BaseScreen

object TabAScreen : BaseScreen() {
    val tabAViewModel: TabAViewModel = TabAViewModel()
    @SuppressLint("StateFlowValueCalledInComposition")
    @Composable
    public fun Base(navController: NavHostController) {
        val viewModel = remember {
            tabAViewModel
        }
        val tabAState by viewModel.tabAState.collectAsState()
        LaunchedEffect(Unit) {
            viewModel.getUrlResponse()
        }
        Column {
            Text(text = tabAState.text)
            Button(onClick = {
                Log.e("wtf", "next")
                navigator(navController = navController, "next")
            }) {

            }
        }
        BackHandler {
            viewModel.updateShowExitDialogMutableState(true)
        }

        if (tabAState.showExitDialogMutableState) {
            CloseAppDialog(viewModel = viewModel)
        }
    }

    @Composable
    public fun CloseAppDialog(viewModel: TabAViewModel) {
        val context = LocalContext.current
        AlertDialog(title = {
            Text(text = "是否想要離開APP")
        }, onDismissRequest = {  }, confirmButton = { 
            TextButton(onClick = {
                viewModel.updateShowExitDialogMutableState(false)
                if (context is Activity) {
                    context.finish()
                }
            }) {
                Text(text = "ok")
            }
        }, dismissButton = {
            TextButton(onClick = {
                viewModel.updateShowExitDialogMutableState(false) }
            ) {
                Text(text = "dismiss")
            }
        })
    }
}

@Preview
@Composable
public fun PreviewTabAScreen() {
    TabAScreen.Base(navController = rememberNavController())
}