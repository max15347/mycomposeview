package com.example.mycomposeview

import androidx.compose.runtime.Composable
import androidx.compose.runtime.Stable
import androidx.compose.runtime.remember
import androidx.navigation.NavHostController
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.example.mycomposeview.route.Route

@Stable
class AppState(
    val navHostController: NavHostController
) {
    val showBackIcon: Boolean
        @Composable get() =
            (
                    when(navHostController.currentBackStackEntryAsState().value?.destination?.route) {
                        Route.TABA.name -> false
                        Route.TABB.name -> false
                        Route.TABC.name -> false
                        else -> true
                    })

    val showBottom: Boolean
        @Composable get() =
            when(navHostController.currentBackStackEntryAsState().value?.destination?.route) {
                Route.TABA.name -> true
                Route.TABB.name -> true
                Route.TABC.name -> true
                else -> false
            }
}

@Composable
fun rememberAppState(
    navHostController: NavHostController = rememberNavController()
) = remember(navHostController) {
    AppState(navHostController)
}