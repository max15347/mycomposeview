package com.example.mycomposeview

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navigation
import com.example.mycomposeview.next.NextScreen
import com.example.mycomposeview.route.Route
import com.example.mycomposeview.taba.TabAScreen
import com.example.mycomposeview.tabb.TabBScreen
import com.example.mycomposeview.tabc.TabCScreen

open class BaseScreen {
    @SuppressLint("StateFlowValueCalledInComposition")
    @Composable
    public fun navHost(navController:NavHostController, startDestination: String) {
        NavHost(modifier = Modifier.fillMaxWidth(), navController = navController, startDestination = startDestination) {
            navigation(
                route = "GG",
                startDestination = startDestination
            ) {

            }
            composable(Route.TABB.name) {
                TabBScreen.Base()
            }
            composable(Route.TABC.name) {
                TabCScreen.Base()
            }
            composable(Route.TABA.name) {
                TabAScreen.Base(navController)
            }
            composable("next") {
                NextScreen.Base()
            }
        }
    }

    public fun navigator(navController:NavHostController, route: String) {
        navController.navigate(route = route)
    }

    public fun present(context:Context, route: String) {
        val intent = Intent(context, PresentActivity::class.java).apply {
            putExtra("route", route)
        }
        context.startActivity(intent)
    }
}