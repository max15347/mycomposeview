package com.example.mycomposeview

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.navigation.compose.rememberNavController
import com.example.mycomposeview.ui.theme.MyComposeViewTheme

class PresentActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        intent.extras?.apply {
            val route = getString("route")
            setContent {
                MyComposeViewTheme {
                    val appState = rememberAppState()
                    PresentScreen.Base(navHostController = appState.navHostController, route = route!!)
                }
            }
        }
    }
}