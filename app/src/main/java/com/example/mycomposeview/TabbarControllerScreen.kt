package com.example.mycomposeview

import android.annotation.SuppressLint
import android.util.Log
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AddCircle
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.material.icons.filled.Call
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material3.DrawerState
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.ModalDrawerSheet
import androidx.compose.material3.ModalNavigationDrawer
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.NavigationDrawerItem
import androidx.compose.material3.NavigationDrawerItemDefaults
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.NavHostController
import androidx.navigation.compose.currentBackStackEntryAsState
import com.example.mycomposeview.route.Route
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

object TabbarControllerScreen : BaseScreen() {
    @SuppressLint("UnrememberedMutableState")
    @Composable
    public fun Base(appState: AppState) {
        Surface(
            modifier = Modifier.fillMaxSize()
        ) {
            NavigationDrawerAndContent(
                appState = appState
            )
        }
    }

    data class DrawerItem(
        val icon: ImageVector,
        val label: String,
        val secondLabel: String
    )

    @Composable
    public fun BackIcon(navController: NavHostController, appState: AppState) {
        if (!appState.showBackIcon) {
            return
        }
        val scope = rememberCoroutineScope()
        IconButton(onClick = {
            scope.launch {
                navController.popBackStack()
            }
        }) {
            Icon(
                imageVector = Icons.Default.ArrowForward,
                contentDescription = "back",
                tint = Color.White
            )
        }
    }

    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    public fun NavigatorIcon(drawerState: DrawerState) {
        val scope = rememberCoroutineScope()
        IconButton(onClick = {
            scope.launch {
                if (drawerState.isOpen) {
                    drawerState.close()
                } else {
                    drawerState.open()
                }
            }
        }) {
            Icon(
                imageVector = Icons.Default.Menu,
                contentDescription = "icon",
                tint = Color.White
            )
        }
    }

    @Composable
    public fun BottomBar(navController: NavHostController, appState: AppState, items: List<String>) {
        if (!appState.showBottom) {
            return
        }
        NavigationBar {
            val navBackStackEntry by navController.currentBackStackEntryAsState()
            val currentDestination = navBackStackEntry?.destination
            items.forEachIndexed { index, item ->
                NavigationBarItem(
                    icon = { Icon(Icons.Filled.Favorite, contentDescription = item) },
                    label = { Text(item) },
                    selected = currentDestination?.hierarchy?.any { it.route == item } == true,
                    onClick = {
                        navigator(navController = navController, route = item)
                    }
                )
            }
        }
    }

}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
public fun NavigationDrawerAndContent(
    appState: AppState,
    lifecycleOwner: LifecycleOwner = LocalLifecycleOwner.current
) {
    val navController = appState.navHostController
    val defaultBottomItems = listOf(Route.TABA.name, Route.TABB.name, Route.TABC.name)
    val drawerState = rememberDrawerState(initialValue = DrawerValue.Closed)
    val drawerItems = listOf(
        TabbarControllerScreen.DrawerItem(
            icon = Icons.Default.Favorite,
            label = "1",
            secondLabel = "第一列"
        ),
        TabbarControllerScreen.DrawerItem(
            icon = Icons.Default.ArrowForward,
            label = "2",
            secondLabel = "第二列"
        ),
        TabbarControllerScreen.DrawerItem(
            icon = Icons.Default.Call,
            label = "3",
            secondLabel = "第三列"
        ),
        TabbarControllerScreen.DrawerItem(
            icon = Icons.Default.AddCircle,
            label = "4",
            secondLabel = "第四列"
        )
    )
    val selectedDrawerItem by remember {
        mutableStateOf(drawerItems[0])
    }
    val scope = rememberCoroutineScope()
    ModalNavigationDrawer(
        drawerState = drawerState,
        drawerContent = {
            ModalDrawerSheet(
                drawerContainerColor = colorResource(id = R.color.white)
            ) {
                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                ) {
                    Text(text = "testTitle")
                }
                drawerItems.forEach {
                    NavigationDrawerItem(
                        colors = NavigationDrawerItemDefaults.colors(
                            selectedContainerColor = colorResource(id = R.color.white),
                            unselectedContainerColor = colorResource(id = R.color.white)
                        ),
                        badge = { Text(text = it.label) },
                        icon = { Icon(imageVector = it.icon, contentDescription = it.label) },
                        label = { Text(text = it.secondLabel) },
                        selected = it == selectedDrawerItem,
                        onClick = {
                            scope.launch {
                                TabbarControllerScreen.present(
                                    navController.context,
                                    route = "next"
                                )
                            }
                        })
                }
            }
        }) {
        Scaffold(
            topBar = {
                TopAppBar(
                    colors = TopAppBarDefaults.centerAlignedTopAppBarColors(
                        containerColor = Color.Red,
                        titleContentColor = Color.White
                    ),
                    title = {
                        Text(text = "GGTitle")
                    },
                    navigationIcon = {
                        TabbarControllerScreen.NavigatorIcon(
                            drawerState = drawerState
                        )
                    },
                    actions = {
                        TabbarControllerScreen.BackIcon(navController, appState)
                    }
                )
            },
            bottomBar = {
                TabbarControllerScreen.BottomBar(
                    navController = navController,
                    appState = appState,
                    items = defaultBottomItems
                )
            }
        ) { innerPadding ->
            Box(
                modifier = Modifier
                    .padding(innerPadding)
                    .fillMaxWidth()
            )
            {
                TabbarControllerScreen.navHost(
                    navController = navController,
                    startDestination = Route.TABA.name
                )
            }
        }
    }
}

@Preview
@Composable
public fun PreviewBase() {
    val appState = rememberAppState()
    TabbarControllerScreen.Base(appState)
}