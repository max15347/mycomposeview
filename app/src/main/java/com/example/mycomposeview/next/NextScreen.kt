package com.example.mycomposeview.next

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonColors
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.Surface
import com.example.mycomposeview.BaseScreen
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.core.content.res.ResourcesCompat
import com.example.mycomposeview.R

object NextScreen : BaseScreen() {
    @Composable
    public fun Base() {
        val showDialog = remember {
            mutableStateOf(false)
        }
        Column {
            Text(text = "next")
            Button(onClick = {
                showDialog.value = true
            }) {
            }
        }
        if (showDialog.value) {
            ShowDialog(onDismiss = {showDialog.value = false})
        }
    }

    @Composable
    public fun ShowDialog(onDismiss:()->Unit) {
        Dialog(onDismissRequest = onDismiss) {
            Surface(
                modifier = Modifier.padding(20.dp),
                shape = RoundedCornerShape(22.dp),
                color = Color.White
            ) {
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally,
                    modifier = Modifier.padding(20.dp, 24.dp)
                ) {
                    Image(painter = painterResource(id = R.drawable.img_payment_error_l), contentDescription = "gg")
                    Text(
                        text = "要跳過問券嗎？",
                        modifier = Modifier.padding(0.dp, 32.dp, 0.dp, 0.dp),
                        color = Color(0xFF404040),
                        fontWeight = FontWeight.Bold,
                        fontSize = 20.sp,
                        textAlign = TextAlign.Center
                    )
                    Text(
                        text = "請協助在回覆期限前完成問卷\n" +
                                "以免影響您使用悠遊付帳戶",
                        color = Color(0xC0404040),
                        fontSize = 16.sp,
                        textAlign = TextAlign.Center,
                        modifier = Modifier.padding(0.dp, 8.dp, 0.dp, 0.dp)
                    )

                    Button(
                        colors = ButtonDefaults.buttonColors(containerColor = Color(0xFF007AC8)),
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(0.dp, 32.dp, 0.dp, 0.dp),
                        onClick = onDismiss) {
                        Text(
                            text = "繼續填寫",
                            fontWeight = FontWeight.Bold,
                            fontSize = 16.sp,
                            color = Color.White,
                        )
                    }

                    Text(
                        text = "跳過",
                        fontSize = 16.sp,
                        fontWeight = FontWeight.Bold,
                        color = Color(0xFF404040),
                        modifier = Modifier.padding(0.dp, 24.dp, 0.dp, 0.dp).clickable {
                            onDismiss()
                        }
                    )
                }
            }
        }
    }
}

@Preview
@Composable
public fun show() {
    NextScreen.Base()
}