package com.example.mycomposeview

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.navigation.compose.rememberNavController
import com.example.mycomposeview.ui.theme.MyComposeViewTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyComposeViewTheme {
                val appState = rememberAppState()
                TabbarControllerScreen.Base(appState)
            }
        }
    }
}